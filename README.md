# Error pages

## Development

```bash
python3 -m http.server
```

## Setup

```bash
sudo rm /usr/share/nginx/html/error.html
sudo ln -s /var/www/error-pages/index.html /usr/share/nginx/html/error.html
```

## Compile Tailwind CSS

```bash
./build.sh
```

## NGINX

```nginx
server {
  listen 80;
  index index.html;

  error_page 500 502 503 504 /error.html;
  location = /error.html {
    root /usr/share/nginx/html;
    internal;
  }
}
```
